CREATE TABLE `webhook-ben`.`issues` (
  `user_id` INT UNSIGNED NOT NULL,
  `issue_id` INT UNSIGNED NOT NULL,
  `title` LONGTEXT NULL,
  `description` LONGTEXT NULL,
  `last_update` DATETIME NULL
  PRIMARY KEY (`user_id`, `repo_id`, `issue_id`));