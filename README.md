# Webhook-relay

Relaying webhook issue event
Detect issues title/description changes
Multiple repository support

## Configuration

The application is configured through `config.json` file. The JSON format file includes MySQL database configuration, repository configuration and credentials, target system configuration and detection interval configuration. Sample configration file can be found at `config.json.sample` or as the following format.

```javascript
{
	"interval": 5,						// Seconds
	"user": "user", 					// Relay webhook sender
	"target": "http://target",			// Target HTTP Server
	"repository": 
	{
		"user/repository":				// Repository owner name repository name
		{
			"secret": "webhook-secret"	// Webhook secret (For calculating signature)
		}
	},
	"database":							// MySQL Database
	{
		"hostname":"localhost",			// Hostname
		"port":"3306",					// Port
		"username":"username",			// Database username
		"password":"password",			// Database password
		"schema":"webhook-ben"			// Schema contains webhook table
	}
}
```

Database table definition/SQL can be found in `database/webhook-ben.sql`.

## Usage

Local execution

```
npm install
npm start
```

Debug (*nix)
```
DEBUG=webhook-relay:* npm start
```

Debug (Windows)
```
set DEBUG=webhook-relay:* & npm start
```
