// Actively monitor and pull issues from GitHub based on user.repository
var crypto = require('crypto');
var request = require('request');
var mysql = require('mysql2');
require('buffer-equal-constant-time').install();


// Constants
const USER_AGENT = "webhook-ben/0";
const GITHUB_REPOS_API = 'https://api.github.com/repos/';
const GITHUB_USERS_API = 'https://api.github.com/users/';
const SELECT_SQL = "SELECT user_id, issue_id, title, description, date_format(last_update, '%Y-%m-%dT%H:%i:%sZ') last_update FROM `webhook-ben`.issues WHERE `issue_id` = :id";
const UPDATE_SQL = "REPLACE INTO `issues` (`user_id`, `issue_id`, `title`, `description`, `last_update`)  VALUES (:user,:issue,:title,:description,STR_TO_DATE(:updatedAt, '%Y-%m-%dT%H:%i:%sZ'))";

function strcmp(strA, strB) {
	// Constant time string buffer comparison
	return new Buffer(strA).equal(new Buffer(strB));
}

function ActiveGitHubMonitor(configuration) {
	// Initialize with configuration
	var self = this;
	self.etags = {};
	self.lastUpdate = {};
	self.repos = configuration.repository;
	self.interval = configuration.interval;
	self.user = configuration.user;
	self.target = configuration.target;

	self.conn = mysql.createConnection({
		host 	: configuration.database.hostname,
		port 	: configuration.database.port,
		user 	: configuration.database.username,
		password: configuration.database.password,
		database: configuration.database.schema
	});
	self.conn.config.namedPlaceholders = true;

};

ActiveGitHubMonitor.prototype.verifyRepo = function(name, secret) {
	// Check repository issues list for changes
	// Compare cached ETag with fetched ETag in HTTP header
	// 304 => Not Modified
	// 200 => Change happens
	var self = this;
	var issue_link = GITHUB_REPOS_API + name + '/issues';
	var headers = { "User-Agent": USER_AGENT };
	if (self.etags[name] !== undefined) headers['If-None-Match'] = self.etags[name];

	request({
		method 	: 'GET',
		uri 	: issue_link,
		headers : headers
	}, function(error, response, body){
		if (error) throw error;
		if (response.statusCode == 200){
			self.verifyIssue(JSON.parse(body), name);
			self.etags[name] = response.headers['etag'];
		}
	});
}

ActiveGitHubMonitor.prototype.verifyIssue = function(issues, name) {
	// Check a single issue for changes in title/description
	// Cached repository data for simulating webhook request
	// Compare title/description with record stored in database
	var self = this;
	var repo_link = GITHUB_REPOS_API + name;
	var headers = { "User-Agent": USER_AGENT };

	request({
		method 	: 'GET',
		uri 	: repo_link,
		headers : headers
	}, function(error, response, body){
		if (error) throw error;
		var repository = JSON.parse(body);
		for (var i in issues) {
			var param = { id: issues[i].id };
			if (self.lastUpdate[issues[i].id] === undefined || !strcmp(self.lastUpdate[issues[i].id], issues[i].updated_at)){
				self.conn.query(SELECT_SQL, param, function(error, result) {
					if (error) throw error;
					if (result.length > 0){
						if (!strcmp(issues[i].title, result[0].title) || !strcmp(issues[i].body, result[0].description)) {
							self.triggerTarget("updated", issues[i], repository, self.sender, name);
						}
					}
					self.updateIssue( issues[i].user.id, issues[i].id, issues[i].title, issues[i].body, issues[i].updated_at);
					self.lastUpdate[issues[i].id] = issues[i].updated_at;
				});
			}
		}
	});
}

ActiveGitHubMonitor.prototype.updateIssue = function(userId, issueId, title, description, updatedAt) {
	// Update database record
	var self = this;
	var param = {
		user 		: userId,
		issue 		: issueId,
		title 		: title,
		description : description,
		updatedAt 	: updatedAt
	}

	self.conn.query(UPDATE_SQL, param, function(error, result) {
		if (error) throw error;
	});
}

ActiveGitHubMonitor.prototype.triggerTarget = function(action, issue, repository, sender, name){
	// Send simulated webhook request to the target system
	// Required:
	//  Calculate signature for authorization
	var self = this;
	var content = { action: action, issue: issue, repository: repository, sender: sender };
	var rawBody = JSON.stringify(content);
	var secret = self.repos[name].secret;
	var signature = crypto.createHmac('sha1', secret).update(rawBody).digest('hex');

	var headers = {
		"X-GitHub-Event"	: "issues",
		"X-Hub-Signature"	: 'sha1='+signature,
		"User-Agent" 		: USER_AGENT,
		"Content-type"		: "application/json"
	};

	request({
		method 	: 'POST',
		uri 	: self.target,
		headers : headers,
		body 	: rawBody
	}, function(error, response, body){
		if (error) throw error;
	});

}

ActiveGitHubMonitor.prototype.run = function() {
	// Run the monitor per interval
	// Cached user data for simulating webhook request
	var self = this;
	var headers = { "User-Agent": USER_AGENT };

	request({
		method 	: 'GET',
		uri 	: GITHUB_USERS_API + self.user,
		headers : headers
	}, function(error, response, body){
		if (error) throw error;
		var user = JSON.parse(body);
		self.sender = {};
		self.sender.login = user.login;
		self.sender.id = user.id;
		self.sender.avatar_url = user.avatar_url;
		self.sender.gravatar_id = user.gravatar_id;
		self.sender.url = user.url;
		self.sender.html_url = user.html_url;
		self.sender.followers_url = user.followers_url;
		self.sender.following_url = user.following_url;
		self.sender.gists_url = user.gists_url;
		self.sender.starred_url = user.starred_url;
		self.sender.subscriptions_url = user.subscriptions_url;
		self.sender.organizations_url = user.organizations_url;
		self.sender.repos_url = user.repos_url;
		self.sender.events_url = user.events_url;
		self.sender.received_events_url = user.received_events_url;
		self.sender.type = user.type;
		self.sender.site_admin = user.site_admin;
		
		for (var repo in self.repos) {
			setInterval(function(){
				self.verifyRepo(repo, self.repos[repo].secret);
			}, self.interval*1000);
		}
	});
}

module.exports = ActiveGitHubMonitor;