var express = require('express');
var crypto = require('crypto');
var request = require('request');
var router = express.Router();
require('buffer-equal-constant-time').install();

 
// Accept POST Request for /webhook
// Relay Webhook to the target system
router.post('/', function(req, res, next) {
	// Verify signature of the HTTP Request
	// For invalid signature => 401 Unauthorized
	var config = req.app.locals.config;
	var repo = req.body.repository.full_name;
	var secret = config.repository[repo].secret;
	var key = crypto.createHmac('sha1', secret).update(req.raw).digest();
	var signature = require('querystring').parse(req.get('X-Hub-Signature'));

	if (!key.equal(new Buffer(signature.sha1, 'hex'))) res.sendStatus(401);
	else next();
}, function (req, res, next){
	// Forward the webhook request to the target system
	var config = req.app.locals.config
	var event = req.get('X-GitHub-Event');
	var action = req.body.action;
	var target = config.target;
	var relayedAgent = "Webhook-Ben-Relayed-"+req.get('User-Agent');
	var headers = {
		"X-GitHub-Event"	: event,
		"X-Hub-Signature"	: req.get('X-Hub-Signature'),
		"X-GitHub-Delivery"	: req.get('X-GitHub-Delivery'),
		"User-Agent"		: relayedAgent,
		"Content-type"		: "application/json"
	};

	request({
		method 	: 'POST',
		uri 	: target,
		headers : headers,
		body 	: req.raw
	}, function(error, response, body){
		if (error) throw error;
		res.sendStatus(200);
	});
});

module.exports = router;
