var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var webhook = require('./routes/webhook');

var monitor = require('./process/ActiveGitHubMonitor');

var app = express();

// Load user configuration files
app.locals.config = require('./config.json');

app.use(logger('dev'));
app.use(bodyParser.json({
  verify:function(req, res, buf){
    req.raw = buf;
  }
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/webhook', webhook);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use(function(err, req, res, next) {
  res.sendStatus(err.status || 500);
});

// Create instance of ActiveGitHubMonitor
var mon = new monitor(app.locals.config);
mon.run();

module.exports = app;
